import bpy
import bmesh
import random
import os
import sys
import json
import math
import time

sys.path.append(os.path.dirname(__file__))

from SphericalForamineraGenerator.GeneratorInputData import GeneratorInputData
from SphericalForamineraGenerator.ForamineraGenerator import ForamineraGenerator

class BlenderInterface(object):

	def prepareScene(self):
		for ob in bpy.context.scene.objects:
			if ob.type == 'MESH':
				ob.select = True
			else: 
				ob.select = False
		bpy.ops.object.delete()

	def close(self):
		bpy.ops.wm.save_as_mainfile(filepath='yolo.blend')

class SelectionHelper(object):

	@classmethod
	def selectOnly(cls, name):
		cls.deselectAllObjects()
		cls.select(name)

	@classmethod
	def select(cls, name):
		bpy.data.objects[name].select = True
		bpy.context.scene.objects.active = bpy.data.objects[name]

	@classmethod
	def deselectAllObjects(cls):
		for ob in bpy.data.objects.values():
			ob.select = False
		bpy.context.scene.objects.active = None

	@classmethod
	def renameSelection(self, newName):
		bpy.context.active_object.name = newName

	@classmethod
	def delete(cls, name):
		cls.selectOnly(name)
		bpy.ops.object.delete()

class StatisticCalculator(object):

	@classmethod
	def getExternalSurafaceArea(cls, objectName, chamberData):
		SelectionHelper.selectOnly(objectName)
		bpy.ops.object.mode_set(mode='EDIT')
		obj = bpy.context.active_object	
		bm = bmesh.from_edit_mesh(obj.data)

		area = 0.0
		for f in bm.faces: 
			f.select = True
			if cls.__checkIfExternalFace(obj, f, chamberData):
				area += f.calc_area()
		bmesh.update_edit_mesh(obj.data)
		# print ("External area", area)
		return area * 100

	@classmethod
	def getTotalSurafaceArea(cls, objectName, chamberData):
		SelectionHelper.selectOnly(objectName)
		bpy.ops.object.mode_set(mode='EDIT')
		obj = bpy.context.active_object	
		bm = bmesh.from_edit_mesh(obj.data)
		area = sum(f.calc_area() for f in bm.faces)	
		# print ("Total area", area)
		return area * 100

	@classmethod
	def getVolume(cls, objectName, chamberData):
		SelectionHelper.selectOnly(objectName)
		bpy.ops.object.mode_set(mode='EDIT')
		obj = bpy.context.active_object	
		bm = bmesh.from_edit_mesh(obj.data)
		voulme = bm.calc_volume()
		# print ("Volume", voulme)
		return voulme * 1000

	@classmethod
	def __checkIfExternalFace(cls, obj, face, chamberData):
		facePosition = obj.matrix_world * face.calc_center_median()
		for chamber in chamberData:
				faceDistance = \
					(facePosition[0] - chamber['position'][0]) ** 2 + \
					(facePosition[1] - chamber['position'][1]) ** 2 + \
					(facePosition[2] - chamber['position'][2]) ** 2
				if math.sqrt(faceDistance) < chamber['scale'][0] - (chamber['thickness']*0.4):
					return False
		return True

class ShellGenerator(object):

	INTERIOR_SCALING_FACTOR = 0.025

	def __init__(self):
		self.chamberGenerator = None

	def setChamberGenerator(self, chamberGenerator):
		self.chamberGenerator = chamberGenerator

	def generate(self, chamberData):
		for idx in range(len(chamberData)):
			self.chamberGenerator.generate(idx, chamberData[idx])
		for idx in reversed(range(1, len(chamberData))):
			self.mergeChambers(idx-1, idx)



	def mergeChambers(self, olderChamberId, youngerChamberId):
		self.generateInteriorCopy(olderChamberId)
		self.__rescale( 
			bpy.data.objects[self.__getChamberInteriorObjectName(olderChamberId)]
		)

		self.__applyBooleanOperation(
			self.__getChamberObjectName(youngerChamberId),  
			self.__getChamberInteriorObjectName(olderChamberId),
			'DIFFERENCE',
			'CARVE',
		)
		self.__applyBooleanOperation(
			self.__getChamberObjectName(olderChamberId),
			self.__getChamberObjectName(youngerChamberId),  
			'UNION',
			'CARVE',
		)
		SelectionHelper.delete(self.__getChamberInteriorObjectName(olderChamberId))
		SelectionHelper.delete(self.__getChamberObjectName(youngerChamberId))


	def generateInteriorCopy(self, chamberId):
		chamberObjectName = self.__getChamberObjectName(chamberId)
		chamberConvexObjectName = self.__getChamberConvexObjectName(chamberId)
		chamberInteriorObjectName = self.__getChamberInteriorObjectName(chamberId)

		self.generateConvexCopy(chamberId)
		SelectionHelper.selectOnly(chamberConvexObjectName)
		bpy.ops.object.duplicate()
		SelectionHelper.renameSelection(chamberInteriorObjectName)
		self.__rescale( 
			bpy.data.objects[chamberInteriorObjectName],
			-self.__getScalingFactor()
		)

		self.__applyBooleanOperation(chamberInteriorObjectName, chamberObjectName, 'DIFFERENCE', 'CARVE')
		SelectionHelper.delete(chamberConvexObjectName)

	def generateConvexCopy(self, chamberId): 
		SelectionHelper.selectOnly('Chamber_{:03d}'.format(chamberId))
		bpy.ops.object.duplicate()
		SelectionHelper.renameSelection('Chamber_{:03d}_Convex'.format(chamberId))
		bpy.ops.object.mode_set(mode='EDIT')
		bpy.ops.mesh.convex_hull()
		bpy.ops.object.mode_set(mode='OBJECT')

	def __applyBooleanOperation(self, mainObjectName, secondaryObjectName, operation, solver=None):
		booleanOperation = bpy.data.objects[mainObjectName].modifiers.new(type='BOOLEAN', name="booleanOperation")
		booleanOperation.operation = operation
		try:
			if solver:
				booleanOperation.solver = solver
		except Exception:
			pass #for older Blender versions
		booleanOperation.object = bpy.data.objects[secondaryObjectName]
		SelectionHelper.selectOnly(mainObjectName)
		bpy.ops.object.modifier_apply(apply_as='DATA', modifier='booleanOperation')

	def __rescale(self, object, scale=None):
		scaleValue = scale or self.__getScalingFactor()
		scale = object.scale
		scale[0] = scale[0] + scaleValue
		scale[1] = scale[1] + scaleValue
		scale[2] = scale[2] + scaleValue

	def __getScalingFactor(self):
		return self.INTERIOR_SCALING_FACTOR 

	def __getChamberObjectName(self, chamberId):
		return 'Chamber_{:03d}'.format(chamberId)

	def __getChamberConvexObjectName(self, chamberId):
		return 'Chamber_{:03d}_Convex'.format(chamberId)

	def __getChamberInteriorObjectName(self, chamberId):
		return 'Chamber_{:03d}_Interior'.format(chamberId)



class ChamberGenerator(object):

	def __init__(self, verticesRatio=1.0):
		self.verticesRatio = verticesRatio


	def generate(self, chamberId, data):
		bpy.ops.mesh.primitive_uv_sphere_add(segments=32*self.verticesRatio, ring_count=16*self.verticesRatio, size=1.0, location=data['position'], view_align=False, enter_editmode=False)
		bpy.context.active_object.name = 'ShellGen_LSphere'
		bpy.data.objects["ShellGen_LSphere"].scale = data['scale']

		bpy.ops.mesh.primitive_uv_sphere_add(segments=32*self.verticesRatio, ring_count=16*self.verticesRatio, size=1.0, location=data['position'], view_align=False, enter_editmode=False)
		bpy.context.active_object.name = 'ShellGen_SSphere'
		bpy.data.objects["ShellGen_SSphere"].scale = list(map(lambda s: s - data['thickness'], data['scale']))

		bpy.ops.mesh.primitive_cylinder_add(vertices=32*self.verticesRatio,depth=2.0, radius=1.0, location=data['aperture'], view_align=False, enter_editmode=False)
		bpy.context.active_object.name = 'ShellGen_Hole'
		bpy.data.objects["ShellGen_Hole"].scale = data['apertureScale']
		bpy.data.objects["ShellGen_Hole"].location = data['aperture']
		bpy.data.objects["ShellGen_Hole"].rotation_euler = data['apertureRotation']

		unionBooleanOperation = bpy.data.objects['ShellGen_SSphere'].modifiers.new(type='BOOLEAN', name="bool 1")
		unionBooleanOperation.operation = 'UNION'
		unionBooleanOperation.object = bpy.data.objects['ShellGen_Hole']
		bpy.data.objects['ShellGen_SSphere'].select = True
		bpy.context.scene.objects.active = bpy.data.objects['ShellGen_SSphere']
		bpy.ops.object.modifier_apply(apply_as='DATA', modifier='bool 1')

		differenceBooleanOperation = bpy.data.objects['ShellGen_LSphere'].modifiers.new(type='BOOLEAN', name="bool 2")
		differenceBooleanOperation.operation = 'DIFFERENCE'
		differenceBooleanOperation.object = bpy.data.objects['ShellGen_SSphere']
		bpy.data.objects['ShellGen_LSphere'].select = True
		bpy.context.scene.objects.active = bpy.data.objects['ShellGen_LSphere']
		bpy.ops.object.modifier_apply(apply_as='DATA', modifier='bool 2')

		bpy.data.objects['ShellGen_LSphere'].select = False
		bpy.ops.object.delete()
		bpy.data.objects['ShellGen_LSphere'].select = True
		bpy.context.active_object.name = 'Chamber_{:03d}'.format(chamberId)
		


def loadData(file):
	with open(file, 'r') as fd:
		return json.loads(fd.read())

bl_info = {
	"name": "ForamineraPlugIn",
	"description": "",
	"author": "",
	"version": (0, 0, 1),
	"blender": (2, 78, 0),
	"location": "3D View > Tools",
	"warning": "", # used for warning icon and text in addons panel
	"wiki_url": "",
	"tracker_url": "",
	"category": "Development"
}

import bpy

from bpy.props import IntProperty, FloatProperty, PointerProperty
from bpy.types import Panel, Operator, PropertyGroup

class GenerateShellOperator(bpy.types.Operator):
	bl_idname = "wm.generate_shell_operator"
	bl_label = "Generate shell"

	shellData = None

	def execute(self, context):
		start = time.time()
		bpy.ops.object.mode_set(mode='OBJECT')
		scene = context.scene
		mytool = scene.foramineraPlugIn

		blender = BlenderInterface()
		blender.prepareScene()
		gen = ForamineraGenerator()
		data = GeneratorInputData(
			mytool.tf, 
			mytool.gf, 
			mytool.fi, 
			mytool.beta, 
			mytool.radius, 
			mytool.thickess, 
			mytool.apertureRadiusPercentage,
			mytool.chamberAmount
		)
		GenerateShellOperator.shellData = gen.generate(data).toJSON()
		shellGenerator = ShellGenerator()
		shellGenerator.setChamberGenerator(ChamberGenerator(mytool.verticesRatio))
		shellGenerator.generate(GenerateShellOperator.shellData)	
		bpy.ops.wm.measure_shell_operator()
		return {'FINISHED'}		

class BisectShellOperator(bpy.types.Operator):
	bl_idname = "wm.bisect_shell_operator"
	bl_label = "Bisect shell"

	def execute(self, context):
			scene = context.scene
			mytool = scene.foramineraPlugIn
			bpy.ops.object.mode_set(mode='EDIT')
			bpy.ops.mesh.select_all('INVOKE_DEFAULT', action='SELECT')
			# obj = bpy.data.objects['Chamber_000']
			# me = obj.data
			# bm = bmesh.from_edit_mesh(me)
			# for face in bm.faces:
			# 	face.select = True
			# for vert in bm.verts:
			# 	vert.select = True
			# for edge in bm.edges:
			# 	edge.select = True
			# bmesh.update_edit_mesh(me)
			# bm.free()
			bpy.ops.mesh.bisect('INVOKE_DEFAULT', False)
			return {'FINISHED'}		

class RemoveBisectScarsOperator(bpy.types.Operator):
	bl_idname = "wm.remove_bisect_scars_operator"
	bl_label = "Remove bisect scars"	

	def execute(self, context):
		scene = context.scene
		mytool = scene.foramineraPlugIn
		bpy.ops.object.mode_set(mode='OBJECT')
		decimateOperation = bpy.context.active_object.modifiers.new(type='DECIMATE', name="remove_bisect_scars")
		decimateOperation.decimate_type = 'DISSOLVE'
		decimateOperation.angle_limit = 0.0174533 #1d
		bpy.ops.object.modifier_apply(apply_as='DATA', modifier='remove_bisect_scars')
		return {'FINISHED'}

class DecimatePreviewShellOperator(bpy.types.Operator):
	bl_idname = "wm.decimate_preview_shell_operator"
	bl_label = "Decimate shell preview"

	def execute(self, context):
		scene = context.scene
		mytool = scene.foramineraPlugIn
		bpy.ops.object.mode_set(mode='OBJECT')
		if 'decimatePreview' not in bpy.context.active_object.modifiers:
			decimateOperation = bpy.context.active_object.modifiers.new(type='DECIMATE', name="decimatePreview")
		else:
			decimateOperation = bpy.context.active_object.modifiers["decimatePreview"]
		decimateOperation.ratio = mytool.ratio
		return {'FINISHED'}	


class DecimateShellOperator(bpy.types.Operator):
	bl_idname = "wm.decimate_shell_operator"
	bl_label = "Confirm"

	def execute(self, context):
		scene = context.scene
		mytool = scene.foramineraPlugIn
		bpy.ops.object.mode_set(mode='OBJECT')
		if 'decimatePreview' not in bpy.context.active_object.modifiers:
			decimateOperation = bpy.context.active_object.modifiers.new(type='DECIMATE', name="decimatePreview")
		else:
			decimateOperation = bpy.context.active_object.modifiers["decimatePreview"]
		decimateOperation.ratio = mytool.ratio
		bpy.ops.object.modifier_apply(apply_as='DATA', modifier='decimatePreview')
		mytool.ratio = 1.0
		return {'FINISHED'}	


class MeasureShellOperator(bpy.types.Operator):
	bl_idname = "wm.measure_shell_operator"
	bl_label = "Measure shell"

	volume = None
	totalSurfaceArea = None
	externalSurfaceArea = None
	x = None
	y = None
	z = None

	@classmethod
	def poll(cls, context):
		return GenerateShellOperator.shellData is not None and 'Chamber_000' in bpy.data.objects

	def execute(self, context):
		scene = context.scene
		mytool = scene.foramineraPlugIn
		MeasureShellOperator.volume = StatisticCalculator.getVolume('Chamber_000', GenerateShellOperator.shellData)
		MeasureShellOperator.totalSurfaceArea = StatisticCalculator.getTotalSurafaceArea('Chamber_000', GenerateShellOperator.shellData)
		MeasureShellOperator.externalSurfaceArea = StatisticCalculator.getExternalSurafaceArea('Chamber_000', GenerateShellOperator.shellData)
		MeasureShellOperator.x = bpy.data.objects['Chamber_000'].dimensions[0]*10
		MeasureShellOperator.y = bpy.data.objects['Chamber_000'].dimensions[1]*10
		MeasureShellOperator.z = bpy.data.objects['Chamber_000'].dimensions[2]*10
		#print (mytool.verticesRatio, len(bpy.context.object.data.vertices))#, MeasureShellOperator.volume, MeasureShellOperator.totalSurfaceArea, MeasureShellOperator.externalSurfaceArea)
		return {'FINISHED'}

class ForamineraSettings(PropertyGroup):


	tf = FloatProperty(
		name = "TF",
		description = "",
		default = 0.4,
		min = 0.00,
		max = 1.0
	)

	gf = FloatProperty(
		name = "GF",
		description = "Growth Factor",
		default = 1.1,
		min = 1.0,
		max = 2.0
	)

	fi = FloatProperty(
		name = u"ΔΦ°",
		description = "",
		default = 71.0,
		min = 0.00,
		max = 360.0
	)

	beta = FloatProperty(
		name = u"β°",
		description = "",
		default = 30.0,
		min = 0.00,
		max = 360.0
	)

	radius = FloatProperty(
		name = u"R[µm]",
		description = "",
		default = 10.0,
		min = 5.00,
		max = 100.0
	)

	thickess = FloatProperty(
		name = "Thickness[µm]",
		description = "",
		default = 2.0,
		min = 1.00,
		max = 10.0
	)

	apertureRadiusPercentage = FloatProperty(		
		name = u"Aperture radius %",
		description = "",
		default = 0.3,
		min = 0.00,
		max = 1.0,
	)

	chamberAmount = IntProperty(
		name = "Chambers",
		description="",
		default = 7,
		min = 1,
		max = 50
	)

	ratio = FloatProperty(		
		name = u"Ratio",
		description = "",
		default = 1.0,
		min = 0.00,
		max = 1.0,
		update = DecimatePreviewShellOperator.execute
	)

	verticesRatio = FloatProperty(		
		name = u"VerticesRatio",
		description = "",
		default = 1.0,
		min = 0.00,
		max = 100.0,
	)



class OBJECT_PT_my_panel(Panel):
	bl_idname = "OBJECT_PT_my_panel"
	bl_label = "Foraminera"
	bl_space_type = "VIEW_3D"   
	bl_region_type = "TOOLS"	
	bl_category = "Tools"
	bl_context = ""

	@classmethod
	def poll(self,context):
		return context.object is not None

	def draw(self, context):
		layout = self.layout
		scene = context.scene
		mytool = scene.foramineraPlugIn
		generatorColumns = layout.column(True)
		generatorColumns.label('Generator params:')
		generatorBox = generatorColumns.box()
		column = generatorBox.column(True)
		column.prop(mytool, "tf")
		column.prop(mytool, "gf") 
		column.prop(mytool, "fi")
		column.prop(mytool, "beta")
		column.prop(mytool, "radius")
		column.prop(mytool, "thickess")
		column.prop(mytool, "apertureRadiusPercentage")
		column.prop(mytool, "chamberAmount")
		column.prop(mytool, "verticesRatio")
		generatorBox.operator("wm.generate_shell_operator")

		measurementColumn = layout.column(True)
		measurementColumn.label('Measurements:')
		measurementBox = measurementColumn.box()
		measurementBoxColumn = measurementBox.column(True)
		measurementBoxColumn.label('Volume: {}'.format(
			"{0:.2f}".format(MeasureShellOperator.volume) if MeasureShellOperator.volume else 'N/A')
		)
		measurementBoxColumn.label('External surface area: {}'.format(
			"{0:.2f}".format(MeasureShellOperator.externalSurfaceArea) if MeasureShellOperator.externalSurfaceArea else 'N/A')
		)
		measurementBoxColumn.label('Total surface Area: {}'.format(
			"{0:.2f}".format(MeasureShellOperator.totalSurfaceArea) if MeasureShellOperator.totalSurfaceArea else 'N/A')
		)
		measurementBoxColumn.label('X-Size: {}'.format(
			"{0:.2f}".format(MeasureShellOperator.x) if MeasureShellOperator.x else 'N/A')
		)
		measurementBoxColumn.label('Y-Size: {}'.format(
			"{0:.2f}".format(MeasureShellOperator.y) if MeasureShellOperator.y else 'N/A')
		)
		measurementBoxColumn.label('Z-Size: {}'.format(
			"{0:.2f}".format(MeasureShellOperator.z) if MeasureShellOperator.z else 'N/A')
		)
		measurementBoxColumn.operator("wm.measure_shell_operator")

		toolsColumns = layout.column(True)
		toolsColumns.label('Tools')
		toolsBox = toolsColumns.box()
		bisectColumn = toolsBox.column(True)
		bisectColumn.operator("wm.bisect_shell_operator")
		bisectColumn.operator("wm.remove_bisect_scars_operator")
		decimateColumn = toolsBox.column(True)
		decimateColumn.label("Decimate")
		decimateColumn.prop(mytool, "ratio")
		#decimateColumn.operator("wm.decimate_preview_shell_operator")
		decimateColumn.operator("wm.decimate_shell_operator")

def register():
	bpy.utils.register_module(__name__)
	bpy.types.Scene.foramineraPlugIn = PointerProperty(type=ForamineraSettings)

def unregister():
	bpy.utils.unregister_module(__name__)
	del bpy.types.Scene.foramineraPlugIn

if __name__ == "__main__":
	register()
