class GeneratorChamberData(object):

    def __init__(self):
        self.radius = None

    def getRadius(self):
        return self.radius

    def setRadius(self, radius):
        self.radius = radius
    