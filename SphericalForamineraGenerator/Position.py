class Position(list):

    def __init__(self, x, y, z):
        super(Position, self).__init__([x, y, z])

    @property
    def x(self):
        return self[0]


    @property
    def y(self):
        return self[1]


    @property
    def z(self):
        return self[2]
