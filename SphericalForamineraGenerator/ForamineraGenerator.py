import math

from ForamineraData import ForamineraData
from GeneratorChamberData import GeneratorChamberData
from ChamberData import ChamberData
from Vector3 import Vector3

class ForamineraGenerator(object):

    UM_TO_3D_UNIT_SCALE = 10
    APERTURE_LOCATION_SEARCH_GRID = 100

    def __init__(self):
        pass

    def generate(self, generatorInputData):
        foramineraData = ForamineraData(generatorInputData)
        for idx in range(generatorInputData.chamberAmount):
            foramineraData.addNewChamber(self.__generateChamber(foramineraData))
        return foramineraData

    def __generateChamber(self, foramineraData):
        if foramineraData.hasAtLeastOneChamber():
            return self.__generateChildChamber(foramineraData)
        else:
            return self.__generateRootChamber(foramineraData)

    def __generateRootChamber(self, foramineraData):
        chamberExternalRadius = foramineraData.getGeneratorInputData().radius/self.UM_TO_3D_UNIT_SCALE
        generatorChamberData = GeneratorChamberData()
        generatorChamberData.setRadius(chamberExternalRadius)
        rootChamber = ChamberData()
        rootChamber.setGeneratorDataObject(generatorChamberData)
        rootChamber.setPosition(Vector3(0, 0, 0))
        rootChamber.setRotation(Vector3(0, 0, 0))
        rootChamber.setAperture(Vector3(chamberExternalRadius, 0, 0))
        apertureVector = rootChamber.getAperture() - rootChamber.getPosition()
        rootChamber.setApertureRotation(Vector3(0, apertureVector.angleZ, apertureVector.angleXY))
        rootChamber.setThickness(foramineraData.getGeneratorInputData().thickness/self.UM_TO_3D_UNIT_SCALE)
        rootChamber.setScale(Vector3(chamberExternalRadius, chamberExternalRadius, chamberExternalRadius))
        rootChamber.setApertureScale(Vector3(
            chamberExternalRadius * foramineraData.getGeneratorInputData().apertureRadiusPercentage, 
            chamberExternalRadius * foramineraData.getGeneratorInputData().apertureRadiusPercentage,
            chamberExternalRadius * foramineraData.getGeneratorInputData().apertureRadiusPercentage)
        )
        return rootChamber

    def __generateChildChamber(self, foramineraData):
        chamberExternalRadius = self.__calculateNextChamberRadius(foramineraData)
        growthVector = self.__calculateGrowthVector(foramineraData)
        previousChamber = foramineraData.getLastChamber()

        generatorChamberData = GeneratorChamberData()
        generatorChamberData.setRadius(chamberExternalRadius)
        childChamber = ChamberData()
        childChamber.setGeneratorDataObject(generatorChamberData)
        childChamber.setPosition(previousChamber.getAperture() + growthVector)
        childChamber.setRotation(Vector3(0, 0, 0))
        childChamber.setAperture(self.__getAperturePosition(foramineraData, childChamber, chamberExternalRadius))
        childChamber.setApertureScale(Vector3(
            chamberExternalRadius * foramineraData.getGeneratorInputData().apertureRadiusPercentage, 
            chamberExternalRadius * foramineraData.getGeneratorInputData().apertureRadiusPercentage,
            chamberExternalRadius * foramineraData.getGeneratorInputData().apertureRadiusPercentage)
        )
        # childChamber.setAperture(Vector3(
        #     previousChamber.getAperture().x + growthVector.x + chamberExternalRadius * (math.sqrt(3)/3),
        #     previousChamber.getAperture().y + growthVector.y + chamberExternalRadius * (math.sqrt(3)/3),
        #     previousChamber.getAperture().z + growthVector.z + chamberExternalRadius * (math.sqrt(3)/3)))
        apertureVector = childChamber.getAperture() - childChamber.getPosition()
        childChamber.setApertureRotation(Vector3(0, apertureVector.angleZ, apertureVector.angleXY))
        childChamber.setThickness(foramineraData.getGeneratorInputData().thickness/self.UM_TO_3D_UNIT_SCALE)
        childChamber.setScale(Vector3(chamberExternalRadius, chamberExternalRadius, chamberExternalRadius))
        return childChamber

    def __getAperturePosition(self, foraminderaData, currentChamber, radius):
        candidate = None
        candidateDistance = float("inf")
        center = currentChamber.getPosition()
        lastAperture = foraminderaData.getChamberAtIndex(-1).getAperture()
        for idx1 in range(self.APERTURE_LOCATION_SEARCH_GRID):
            for idx2 in range(self.APERTURE_LOCATION_SEARCH_GRID):
                newCandidate = Vector3(
                    center.x + (
                        radius *
                        math.sin(math.pi * idx2 / self.APERTURE_LOCATION_SEARCH_GRID) *
                        math.cos(2 * math.pi * idx1 / self.APERTURE_LOCATION_SEARCH_GRID)
                    ),
                    center.y + (
                        radius *
                        math.sin(math.pi * idx2 / self.APERTURE_LOCATION_SEARCH_GRID) *
                        math.sin(2 * math.pi * idx1 / self.APERTURE_LOCATION_SEARCH_GRID)
                    ),
                    center.z + (
                        radius *
                        math.cos(math.pi * idx2 / self.APERTURE_LOCATION_SEARCH_GRID)
                    )
                )
                distance = newCandidate.distanceApprox(lastAperture)
                if distance < candidateDistance and self.__checkCandidatePosition(newCandidate, foraminderaData):
                    candidate = newCandidate
                    candidateDistance = distance
        return candidate

    def __checkCandidatePosition(self, candidate, foraminderaData):
        for chamberIdx in range(foraminderaData.getChamberAmount()):
            chamber = foraminderaData.getChamberAtIndex(chamberIdx)
            if candidate.distanceApprox(chamber.getPosition()) < chamber.getGeneratorDataObject().getRadius():
                return False
        return True

    def __calculateNextChamberRadius(self, foramineraData):
        previousChamberGeneratorData = foramineraData.getLastChamberGeneratorData()
        return previousChamberGeneratorData.getRadius() * foramineraData.getGeneratorInputData().gf

    def __calculateGrowthVector(self, foramineraData):
        previousChamber = foramineraData.getLastChamber()
        #case with only root chamber
        if foramineraData.getChamberAmount() == 1:
            referenceVector = previousChamber.getAperture() - previousChamber.getPosition()
        else:
            prev2Chamber = foramineraData.getChamberAtIndex(-2)
            referenceVector = previousChamber.getAperture() - prev2Chamber.getAperture()
        growthVector = Vector3(referenceVector.x, referenceVector.y, referenceVector.z)
        if foramineraData.getChamberAmount() >= 3:
            prev2Chamber = foramineraData.getChamberAtIndex(-2)
            prev3Chamber = foramineraData.getChamberAtIndex(-3)
            growthHelperVector = prev2Chamber.getAperture() - prev3Chamber.getAperture()
        else:
            growthHelperVector = Vector3(0, 1, 0)
        normalPlaneVector = Vector3.crossVectors(growthVector,  growthHelperVector)
        normalPlaneVector.normalize()
        growthVector = growthVector.applyAxisAngle(normalPlaneVector, foramineraData.getGeneratorInputData().fi)
        growthVector = growthVector.applyAxisAngle(referenceVector, foramineraData.getGeneratorInputData().beta)
        growthVector.setLength(previousChamber.getGeneratorDataObject().radius * foramineraData.getGeneratorInputData().tf)
        return growthVector

if __name__ == '__main__':
    import json
    from GeneratorInputData import GeneratorInputData


    class PythonObjectEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, (list, dict, str, unicode, int, float, bool, type(None))):
                return json.JSONEncoder.default(self, obj)
            return obj.toJSON()

    data = GeneratorInputData(0.4, 1.1, math.radians(71), math.radians(30), 10, 1.0, 8)
    gen = ForamineraGenerator()
    with open('data.json', 'w') as fd:
        fd.write(json.dumps(gen.generate(data), cls=PythonObjectEncoder))


