class ChamberData(object):

    def __init__(self):
        self.genaratorDataObject = None
        self.position = None
        self.rotation = None
        self.scale = None
        self.aperture = None
        self.apertureScale = None
        self.apertureRotation = None
        self.thickness = None

    def getGeneratorDataObject(self):
        return self.genaratorDataObject

    def setGeneratorDataObject(self, genaratorDataObject):
        self.genaratorDataObject = genaratorDataObject

    def getRadius(self):
        return self.radius

    def setRadius(self, radius):
        self.radius = radius

    def getPosition(self):
        return self.position

    def setPosition(self, position):
        self.position = position

    def getRotation(self):
        return self.rotation

    def setRotation(self, rotation):
        self.rotation = rotation

    def getScale(self):
        return self.scale

    def setScale(self, scale):
        self.scale = scale

    def getAperture(self):
        return self.aperture

    def setAperture(self, aperture):
        self.aperture = aperture

    def getApertureScale(self):
        return self.apertureScale

    def setApertureScale(self, apertureScale):
        self.apertureScale = apertureScale

    def getApertureRotation(self):
        return self.apertureRotation

    def setApertureRotation(self, apertureRotation):
        self.apertureRotation = apertureRotation

    def setThickness(self, thickness):
        self.thickness = thickness

    def getThickness(self):
        return self.thickness

    def toJSON(self):
        return {
            'position': self.getPosition(),
            'scale': self.getScale(),
            'rotation': self.getRotation(),
            'aperture': self.getAperture(),
            'apertureScale': self.getApertureScale(),
            'apertureRotation': self.getApertureRotation(),
            'thickness': self.getThickness()
        }

