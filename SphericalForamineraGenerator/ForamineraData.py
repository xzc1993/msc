class ForamineraData(object):

    def __init__(self, generatorInputData):
        self.generatorInputData = generatorInputData
        self.chambers = list()

    def getGeneratorInputData(self):
        return self.generatorInputData

    def getChamberAmount(self):
        return len(self.chambers)

    def hasAtLeastOneChamber(self):
        return len(self.chambers)

    def getChamberAtIndex(self, index):
        return self.chambers[index]

    def getLastChamber(self):
        return self.getChamberAtIndex(-1)

    def getLastChamberGeneratorData(self):
        return self.getLastChamber().getGeneratorDataObject()

    def addNewChamber(self, chamber):
        self.chambers.append(chamber)
        
    def toJSON(self):
        return [chamber.toJSON() for chamber in self.chambers]

