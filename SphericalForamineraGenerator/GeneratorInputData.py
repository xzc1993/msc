class GeneratorInputData(object):

    def __init__(self, tf, gf, fi, beta, radius, thickess, apertureRadiusPercentage, chamberAmount):
        self.tf = tf
        self.gf = gf
        self.fi = fi
        self.beta = beta
        self.radius = radius
        self.thickness = thickess
        self.apertureRadiusPercentage = apertureRadiusPercentage
        self.chamberAmount = chamberAmount
        