import numpy as np
import math


class Vector3(list):

    def __init__(self, x, y, z):
        super(Vector3, self).__init__([x,y,z])

    @classmethod
    def crossVectors(cls, a, b):
        return Vector3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x)

    @property
    def x(self):
         return self[0]

    @property
    def y(self):
        return self[1]

    @property
    def z(self):
        return self[2]

    @property
    def length(self):
        return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)

    @property
    def angleXY(self):
        if self.x:
            return math.atan2(self.y, self.x)
        else:
            return math.radians(90)

    @property
    def angleZ(self):
        return math.acos(self.z/self.length)

    def __imul__(self, a):
        self[0] *= a
        self[1] *= a
        self[2] *= a

    def __add__(self, other):
        return Vector3(self.x + other.x, self.y + other.y, self.z + other.z)

    def __sub__(self, other):
        return Vector3(self.x - other.x, self.y - other.y, self.z - other.z)

    def normalize(self):
        self.setLength(1)
        return self

    @classmethod
    def crossVectors(cls, a, b):
        return Vector3(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x)

    def distanceApprox(self, vectorB):
        return (self.x - vectorB.x) ** 2 + (self.y - vectorB.y) ** 2 + (self.z - vectorB.z) ** 2

    def setLength(self, newLenght):
        if(self.length):
            self *= (newLenght / self.length)
        return self
        
    def applyAxisAngle(self, axis, angle):
        return Vector3(*np.dot(self.__getRotationMatrix(axis, angle), self))

    def __getRotationMatrix(self, axis, angle):
        axis = np.asarray(axis)
        try:
            axis = axis/math.sqrt(np.dot(axis, axis))
        except RuntimeWarning as e:
            raise Exception(e)
        a = math.cos(angle/2.0)
        b, c, d = -axis*math.sin(angle/2.0)
        aa, bb, cc, dd = a*a, b*b, c*c, d*d
        bc, ad, ac, ab, bd, cd = b*c, a*d, a*c, a*b, b*d, c*d
        return np.array([[aa+bb-cc-dd, 2*(bc+ad), 2*(bd-ac)],
                         [2*(bc-ad), aa+cc-bb-dd, 2*(cd+ab)],
                         [2*(bd+ac), 2*(cd-ab), aa+dd-bb-cc]])

    def toJSON(self):
        return [self.x, self.y, self.z]


