# README #

### Foraminifera ###

Foraminifera is Blender plugin designed to generate and visualize Foraminifera shells in form of single 3D mesh. It offers capability to measure basic physical properties:

+ Total surface area
+ External surface area
+ Material volume

Tool is master thesis project.

### How do I get set up? ###

Plugin was tested on Windows 7 and Ubuntu 16.04. It should run properly also on other platforms supporting Blender.

1. Install Blender. Plugin was tested with version 2.76b and 2.79.
1. Add blender to system path
1. Run script with command: blender --python $Path_to_repository$/main.py

### Who do I talk to? ###

Author: Krzysztof Zięba

Thesis supervisor: dr inż. Paweł Topa